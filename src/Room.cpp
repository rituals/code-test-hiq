#include "Room.h"

Room::Room(int w, int h) {
    if (w > ROOM_MAX_WIDTH || h > ROOM_MAX_WIDTH) {
        cout << "🤯  Room too big! Max width: " << ROOM_MAX_WIDTH << ", max height: " << ROOM_MAX_HEIGHT << endl;
        exit(0);
    }

    width = w;
    height = h;
}

bool Room::inside_room(int x, int y) {
    return (x >= 0 && y >= 0 && x <= width && y <= height);
}


