#include "Simulator.h"

using namespace std;

void Simulator::start_simulation() {
    cout << endl << "Hello and welcome to the HiQ Car Simulator 3000!" << endl;
    cout << "First off, let's build a room:" << endl;

    build_room();

    cout << endl << "Now we need to place out our car (I've chosen a monster truck):" << endl;
    build_car();

    cout << endl << "Let's build a sequence to test! (F)orward, (B)ackwars, (R)ight, (L)eft: ";
    build_sequence();

    cout << endl << "All set! Here we go! Counting down from" << endl;
    sleep(1);
    cout << "3" << endl;
    sleep(1);
    cout << "2" << endl;
    sleep(1);
    cout << "1" << endl;
    sleep(1);

    run_sequence();
}

void Simulator::build_room() {
    int width, height;
    cout << "Width of room: ";
    cin >> width;

    cout << "Height of room: ";
    cin >> height;

    room = new Room(width, height);
}

void Simulator::build_car() {
    int posX, posY;
    char direction;

    cout << "Position X of car: ";
    cin >> posX;

    cout << "Position Y of car: ";
    cin >> posY;

    if (!room->inside_room(posX, posY)) {
        // ERROR
        cout << "🚨 Car outside room!" << endl;
        exit(0);
    }

    cout << "Direction of car (N, E, S, W): ";
    cin >> direction;

    car = new Car(posX, posY, direction);
}

void Simulator::build_sequence() { 
    cin >> sequence;
}

void Simulator::run_sequence() {
    for (unsigned long i = 0; i < sequence.length(); i++) {
        char c = sequence[i];
        switch(toupper(c)) {
            case 'F': car->move_forward(); break;
            case 'B': car->move_backward(); break;
            case 'R': car->turn('R'); break;
            case 'L': car->turn('L'); break;

            // ERROR
            default: cout << "❗️  Invalid token found at " + to_string(i) + " (0-index). But I just skipped it." << endl;
        }
        if (!room->inside_room(car->get_position_x(), car->get_position_y())) {
            cout << "🚑  Your sequence caused a car crash! Please check your sequence at " + to_string(i) + "!" << endl;
            exit(0);
        }
    }

    // SUCCESS
    cout << "🌟  Hey, you made it! Good sequence! You car is at position X:" + to_string(car->get_position_x()) + ", Y:" + to_string(car->get_position_y()) << endl;
}