#include "Simulator.h"

using namespace std;

int main() {
	Simulator *simulator = new Simulator();

	simulator->start_simulation();
	return 0;
}