#include "Car.h"

Car::Car (int posX, int posY, char direction) {
    set_position(posX, posY);
    set_direction(direction);
}

void Car::set_position(int x, int y) {
    posX = x;
    posY = y;
}

void Car::set_direction(char input_direction) {
    switch (toupper(input_direction)) {
        case 'N': direction = 0; break;
        case 'E': direction = 1; break;
        case 'S': direction = 2; break;
        case 'W': direction = 3; break;
        default: cout << "⛔️ Not a valid direction." << endl; exit(0);
    }
}

void Car::turn(char turn) {
    switch (toupper(turn)) {
        case 'R': direction = (direction + 1) % 4; break;
        case 'L': direction = abs(direction - 1) % 4; break;
    }
}

void Car::move_forward() {
    switch(direction) {
        case 0: set_position(posX, posY+1); break;
        case 1: set_position(posX+1, posY); break;
        case 2: set_position(posX, posY-1); break;
        case 3: set_position(posX-1, posY); break;
    }
}

void Car::move_backward() {
    switch(direction) {
        case 0: set_position(posX, posY-1); break;
        case 1: set_position(posX-1, posY); break;
        case 2: set_position(posX, posY+1); break;
        case 3: set_position(posX+1, posY); break;
    }
}