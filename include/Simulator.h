#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <iostream>
#include <string>
#include <unistd.h>
#include "Room.h"
#include "Car.h"

using namespace std;

class Simulator {
    Room *room;
    Car *car;
    string sequence;

    void build_room();
    void build_car();
    void build_sequence();
    void run_sequence();

    public:
        void start_simulation();
};

#endif // SIMULATOR_H