#ifndef ROOM_H
#define ROOM_H

#include <iostream>
#include <string>
#include "_settings.h"

using namespace std;

class Room {
    int width;
    int height;

    public:
        Room (int, int);
        bool inside_room (int, int);
};

#endif // ROOM_H