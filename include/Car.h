#ifndef CAR_H
#define CAR_H

#include <iostream>

using namespace std;

class Car {
    int posX,
        posY;

    int direction;  // 0 - North,
                    // 1 - East,
                    // 2 - South,
                    // 3 - West

    public:
        Car (int, int, char);
        void set_position(int, int);
        void set_direction(char);
        void move_forward();
        void move_backward();
        void turn(char);

        int get_position_x() { return posX; };
        int get_position_y() { return posY; };
};

#endif // CAR_H