# Car Simulator 3000
Author: Robin Jonsson (hejrobinjonsson@gmail.com)

## Run project
There is an executable in the bin folder!

## Details about the project
This project is written in C++ using Visual Studio code in OS X. It took me about 5 active hours.

Every input is case insensitive and error handling is rough when it needs to and gentle when it can. 